$(document).ready(function() {

	function readUrl(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				$('.logo_img img').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0])
			return input.files[0].name;
		}
	}
	$('input[name="logo"]').change(function(){
		readUrl(this);
	});

	$('input[name="header"]').change(function(){
		readUrl(this);
	});

$('body').on('click', 'img.del', function(){
				$(this).parent().parent().remove();
				var id = $(this).parent().children('input[name="id"]').val();
				$.ajax({
					url:'admin/ajaxDel',
					method:'post',
					data:{id:id},
					success:function(){
						deleteImgPosition();
					}
				});
			});
$('body').on('click', 'img.edit', function(){
	var trId = $(this).parent().parent().index()+1;
	$('input[name="trIdForm"]').val(trId);
	var id = $(this).parent().children('input[name="id"]').val();
	var link = $(this).parent().parent().children('#tdlink').text();
	
	$.ajax({
		url:'admin/ajaxEdit',
		method:'post',
		data:{id:id},
		success:function(res)
		{
			$('#editModal').modal('show');
			$('input[name="name"]').val(res);
		}
	});
	$('input[name="hiddenId"]').val(id);
	$('input[name="link"]').val(link);
});

$("#submitEdit").click(function(){
				var id = $("input[name='hiddenId']").val();
				var name = $("input[name='name']").val();	
				var link = $("input[name='link']").val();			
				if((name != '') & (link != ''))
					$.ajax({
						url:'admin/edit',
						data:{id:id,name:name,link:link},
						method:'post',
						success:function(res){
							$("#editModal").modal("hide");
							var tr = $('tr');
							var trId = $("input[name='trIdForm']").val();
							$(tr[trId]).children("#tdname").text(name);
							$(tr[trId]).children("#tdlink").text(link);	
						}
					});
				else alert('Ви не заповнили обовязкові поля');
				
				
			});	

$('.addspan').click(function(){
	$('#addModal').modal('show');
});

$('#submitAdd').click(function(){
	var name = $('input[name="addname"]').val();
	var link = $('input[name="addlink"]').val();
	var insert_id = 0;
	if(name != '')
	{
		
		$.ajax({
		method:'POST',
		url:'/admin/addlink',
		async:'false',
		data:{name:name, link:link},
		success:function(res){
			insert_id = res;
			$('.list_menu tr:last').children('#conf').children('.down').css('display', 'inline', 'margin', '0px');
			$('.list_menu tr:last').children('#conf').children('.up').css('margin-right', '0px');
			$('.list_menu').append('<tr><td id="tdname">'+name+'</td><td id="tdlink">'+link+'</td><td id="conf"><input type="hidden" name="trId" value="0"><input type="hidden" name="id" value="'+insert_id+'"><img src="/assets/img/up.png" class="up"><img src="/assets/img/down.png" class="down"><img src="/assets/img/edit.png" class="edit" alt=""><img src="/assets/img/del.png" class="del" alt=""></td</tr>'); 
			
			$('#addModal').modal('hide');
			$('.list_menu tr:last').children('#conf').children('.down').css('dislay', 'none');
			updateAllIndex();
			deleteImgPosition();
			}
		});
		
	}
	
	
});

	function deleteImgPosition()
	{
		$('.list_menu tr:first').children('#conf').children('.up').css('display', 'none');
		$('.list_menu tr:first').children('#conf').children('.down').css('margin-left', '30px');
		$('.list_menu tr:last').children('#conf').children('.down').css('display', 'none');
		$('.list_menu tr:last').children('#conf').children('.up').css('margin-right', '30px');
	}
	
	deleteImgPosition();
	

	function updateAllIndex()
	{
		var mas = [];
		var i = -1;
		$('tbody tr').each(function(){
		i +=1;
		var id = $(this).children('#conf').children('input[name="id"]').val();
		mas[i] = id;	
		});
		$.ajax({
			method:'POST',
			url:'/admin/mas',
			data:{mas:mas},
			success:function(res){
			
			}
		});
	}
	updateAllIndex();

	$('.default-logo').click(function(){
		$.ajax({
			method: 'POST',
			url: 'defaultLogo'
		});
		document.location.replace('/admin');
		return false;
	});
	$('.default-header').click(function(){
		$.ajax({
			method:'POST',
			url:'defaultHeader'				
		});
		document.location.replace('/admin');
		return false;
	});

	$('.up').on('click', function(){
		$('.list_menu tr:last').index(0);
	});

	$('.down').click(function(){
		var id = $(this).parent().children('input[name="id"]').val();
		var position = $(this).parent().parent().index();
		var nextposition = position + 1; 
		var nextid = $('tbody tr').eq(nextposition).children('#conf').children('input[name="id"]').val();
		$.ajax({
			method : 'POST',
			url : 'admin/change',
			data : {id:id, position:position, nextposition:nextposition, nextid:nextid},
			success : function(res){
				document.location.replace('/admin');
			}
		});
	});

	$('.up').click(function(){
		var id = $(this).parent().children('input[name="id"]').val();
		var position = $(this).parent().parent().index();
		var nextposition = position - 1; 
		var nextid = $('.list_menu tr').eq(nextposition).children('#conf').children('input[name="id"]').val();
		$.ajax({
			method : 'POST',
			url : 'admin/change',
			data : {id:id, position:position, nextposition:nextposition, nextid:nextid},
			success : function(res){
				document.location.replace('/admin');
			}
		});
		
	});
	
	$('.add-header').click(function(){
		$('#addHeaderModal').modal('show');
		return false;
	});
	$('.submit-new-header').click(function(){
		var name = document.getElementById('input-file-header').files[0].name;
		var name= $("#input-file-header").val();
         if(name!=" "){  
			$('.form-header').submit();
			return true;
            }
        return false;		
	});
	
	
				
	$('body').on('click', '.hdel', function(){

		var id = $(this).parent().children('input[name="hid"]').val();
		$.ajax({
			url:'/admin/deleteHeader',
			method:'post',
			data:{id:id},
			success:function(res){
				alert(res);
			}
		});
		$(this).parent().parent().remove();
	});
	
	$('body').on('click', '.hedit', function(){
		var id = $(this).parent().children('input[name="hid"]').val();
		var caption = $(this).parent().parent().children('#hcaption').text();
		var text = $(this).parent().parent().children('#htext').text();
		var link = $(this).parent().parent().children('#hlink').text();
		var linktext = $(this).parent().parent().children('#hlinktext').text();
		$('input[name="hid"]').val(id);
		$('input[name="hcaption"]').val(caption);
		$('input[name="htext"]').val(text);
		$('input[name="hlink"]').val(link);
		$('input[name="hlinktext"]').val(linktext);
		$('#editHeaderModal').modal('show');		
	});
	
	$('.submit-edit-header').click(function(){
		var id = $('input[name="hid"]').val();
		var caption = $('input[name="hcaption"]').val();
		var text = $('input[name="htext"]').val();
		var link = $('input[name="hlink"]').val();
		var linktext = $('input[name="hlinktext"]').val();
		$.ajax({
			method:'post',
			url:'/admin/editHeader',
			data:{id:id, caption:caption, text:text, link:link, linktext:linktext},
			success:function(res){
				
			}
		});
	});
	
});