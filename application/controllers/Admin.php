<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function editHeader()
	{
		$this->load->model('header_model');
		$this->header_model->edit();
	}
	
	public function deleteHeader()
	{
		$this->load->model('header_model');
		$this->header_model->delete();
	}

	public function mas()
	{
		$this->load->model('menu_model');
		$this->menu_model->updatePosition();
		
	}

	public function change()
	{
		$this->load->model('menu_model');
		$this->menu_model->change();
	}

	public function defaultLogo()
	{
		$this->load->model('logo_model');
		$this->logo_model->defaultL();
	}	

	public function defaultHeader()
	{
		$this->load->model('header_model');
		$this->header_model->defaultH();
	}

	public function addlink()
	{
		$this->load->model('menu_model');
		$id = $this->menu_model->add();
		echo $id;
	}

	public function edit()
	{
		$this->load->model('menu_model');
		$this->menu_model->edit();
	}

	public function updateLogo()
	{
				$config['upload_path']          = './upload/';
                $config['allowed_types']        = 'gif|jpg|png|ico';
                $config['max_size']             = 0;
                $config['max_width']            = 0;
                $config['max_height']           = 0;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        redirect('admin/index');
                }
                else
                {
                    $data = $this->upload->data();
                	$this->load->model('logo_model');
					$this->logo_model->update($data['file_name']);
					redirect('admin/index');
                }		
			
	}

	public function updateHeader()
	{
				$config['upload_path']          = './upload/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 0;
                $config['max_width']            = 0;
                $config['max_height']           = 0;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        redirect('admin/index');
                }
                else
                {
                    $data = $this->upload->data();
                	$this->load->model('header_model');
					$this->header_model->add($data['file_name']);
					//redirect('admin/index');
                }		
			
	}

	public function ajaxDel()
	{
		$this->load->model('menu_model');
		$this->menu_model->del();
		
	}

	public function ajaxEdit()
	{
		$this->load->model('menu_model');
		$res = $this->menu_model->nameForEdit();
		$name = '';
		foreach($res as $i)
		{
			$name = $i->name;
			
		}
		echo $name;
		
	}

	public function logout()
	{
		$this->session->set_userdata('email','');
		redirect('admin/login');
	}

	public function index()
	{
		if($this->session->userdata('email') == '')
			redirect('admin/login');
		$this->load->model('menu_model');
		$array['menu'] = $this->menu_model->get_all();
		$this->load->model('logo_model');
		$array['logo'] = $this->logo_model->get();
		$this->load->model('header_model');
		$array['header'] = $this->header_model->get();
		return $this->load->view('admin/index', array('array' => $array));
	}

	public function login()
	{
		$this->session->set_userdata('email', '');
		$this->load->library('form_validation');
		$config = array(
			array(
				'field' =>'email',
				'label' => 'Email',
				'rules' => 'required'
				),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
				),	
			);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE)
		{
			$error['error'] = validation_errors();
			return $this->load->view('admin/login', $error);
		}
		else 
		{
			$this->load->model('admin_model');
			if($this->admin_model->is_user($this->input->post('email'), $this->input->post('password')))
			{
				$this->set_session();
				
				redirect('admin/index');
			}
			else 
			{
				$error['error'] = 'Невірний логін або пароль!';
				return $this->load->view('admin/login',$error);
			}
			
		}
		return $this->load->view('admin/login');
	}

	private function set_session()
	{
		$this->session->set_userdata('email', $this->input->post('email'));
	}

	private function send_to_mail($password)
	{
		$to = $this->input->post('email');
		$subject = 'Відновлення паролю';
		$message = '
		<!DOCTYPE html>
		<head>
		<>
		</head>
		<body>
			<h1>Ваш новий пароль</h1>
			<p>'.$password.'</p>
		</body>
		</html>
		';
		$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
		$headers .= "From: DivArt\r\n";
		mail($to, $subject, $message, $headers); 
	}

	public function reaccess()
	{
		$this->load->library('form_validation');
		$config = array(
			array(
				'field' =>'email',
				'label' => 'Email',
				'rules' => 'required'
				),
			);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE)
		{
			$error['error'] = '';
			return $this->load->view('admin/reaccess', $error);
		}
		else 
		{
			$new_password = $this->generate_password();
			$this->load->model('admin_model');
			if($this->admin_model->reaccess_user($this->input->post('email'),$new_password))
			{
				$this->send_to_mail($new_password);
				$this->set_session();
				echo 'Your new password: <b>'.$new_password.'<br><a href="index">Для продовження перейдіть по даній силці</a>';
			}
			else 
			{
				$error['error'] = 'Даний email ще не іннує! Будь-ласка, зареєструйтесь спочатку.';
				return $this->load->view('admin/reaccess', $error);
			}
			
		}
		

	}

	public function reg()
	{
		$this->session->set_userdata('email', '');
		$this->load->library('form_validation');
		$config = array(
			array(
				'field' =>'email',
				'label' => 'Email',
				'rules' => 'required|valid_email|is_unique[users.email]'
				),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
				),	
			);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE)
		{
			return $this->load->view('admin/reg');
		}
		else 
		{
			$this->load->model('admin_model');
			if($this->admin_model->registration($this->input->post('email'), $this->input->post('password')))
			{
				$this->set_session();
				redirect('admin/index');
			}
			else 
			{
				return $this->load->view('admin/login');
			}
			
		}
		return $this->load->view('admin/login');
	}

	private function generate_password()
	{
		$abetka = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$autopassword = array();
		$length = strlen($abetka) - 1;	
		for($i = 0; $i < 9; $i++)
		{
			$n = rand(0, $length);
			$autopassword[] = $abetka[$n]; 
		}
		$pass = implode($autopassword);
		return $pass;
	}

}
