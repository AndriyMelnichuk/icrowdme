<?php 
class Menu_model extends CI_Model {        

        public $id;
        public $name;
        public $link;
        public $visible;
        public $position;

        public function change()
        {
            $data1 = array(
                'position' => $_POST['nextposition'],
                );
            $this->db->where('id', $_POST['id']);
            $this->db->update('menu', $data1);
            $data2 = array(
                'position' => $_POST['position'],
                ); 
            $this->db->where('id', $_POST['nextid']);
            $this->db->update('menu', $data2);
        }

        public function updatePosition()
        {
            foreach($_POST['mas'] as $index=>$id)
            {
                $data = array(
                    'position' => $index
                ); 
                $this->db->where('id', $id);
                $this->db->update('menu', $data);
            }
            
        }

        public function nameForEdit()
        {
            
            $query = $this->db->get_where('menu', array('id' => $_POST['id']));
            $res = $query->result();
            return $res;
        }

        public function edit()
        {
            $data = array(
                'name' => $_POST['name'],
                'link' => $_POST['link']
                );
            $this->db->where('id', $_POST['id']);
            $this->db->update('menu', $data);
        }

        public function get_menu(){
            $this->db->select('name, link');
            $this->db->order_by('position', 'ASC');
            $query = $this->db->get('menu');
            $res = $query->result();
            return $res;
        }

        public function get_all()
        {
            $this->db->select('*');
            $this->db->order_by('position', 'ASC');
            $query = $this->db->get('menu');
            $res = $query->result();
            return $res;
        }

        public function del()
        {
            $this->db->delete('menu', array('id' => $_POST['id']));
        }
        
        public function add()
        {
            $this->name = $_POST['name'];
            $this->link = $_POST['link'];
            $this->visible = 1;
            $this->position = 0;
            $this->db->insert('menu', $this);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        

        
}