<?php 
class Logo_model extends CI_Model {        

        public function defaultL()
        {
            $data = array(
                'name' => 'logo.png'
                );
            $this->db->where('id',1);
            $this->db->update('logo', $data);
        }

        public function get()
        {
            $this->db->where('id', 1);
            $query = $this->db->get('logo');
            $res = $query->result();
            return $res;
        }

        public function update($name)
        {
            $data = array(
                'name' => $name
                );
            $this->db->where('id', 1);
            $this->db->update('logo', $data);
        }
        
}