<?php 
class Header_model extends CI_Model {        

        public $id;
        public $name;
        public $position;
        public $caption;
        public $text;
        public $link;
        public $linktext;

        public function defaultH()
        {
            $this->db->empty_table('header');
            $this->name = 'header-bg.jpg';
            $this->db->where('id', 1);
            $this->db->update('header', $this);
        }

        public function get()
        {
            $query = $this->db->get('header');
            $res = $query->result();
            return $res;
        }

        public function add($name)
        {
            $this->name = $name;
            $this->position = 1;
            $this->caption = $_POST['caption'];
            $this->text = $_POST['text'];
            $this->link = $_POST['link'];
            $this->linktext = $_POST['linktext'];
            $this->db->insert('header', $this);
        }

        public function delete()
        {
            
            $this->db->delete('header', array('id' => $_POST['id']));
        }
        
        public function edit()
        {
            $this->caption = $_POST['caption'];
            $this->text = $_POST['text'];
            $this->link = $_POST['link'];
            $this->linktext = $_POST['linktext'];
            $data = array(
                'caption' => $_POST['caption'],
                'text' => $_POST['text'],
                'link' => $_POST['link'],
                'linktext' => $_POST['linktext']
                );
            $this->db->where('id', $_POST['id']);
            $this->db->update('header', $this);
        }
}