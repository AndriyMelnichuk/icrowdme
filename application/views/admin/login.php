<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" href="/assets/css/form.css">
</head>
<body>
	<div class="content">
		<div class="logo">
			<center><img src="/assets/img/logo.png" alt="logo"></center>			
		</div>
	
	<?php $attributes = array('class' => 'form-horizontal');?>
	<?php echo form_open('admin/login', $attributes);?>
	<?php 
	echo '<p>'.$error.'</p>';
	?>
  <div class="form-group">
    <label class="control-label col-sm-3" for="email">Email:</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3" for="pwd">Password:</label>
    <div class="col-sm-9"> 
      <input type="password" class="form-control" name="password" id="pwd" placeholder="Enter password">
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-3 col-sm-9">
      	<button type="submit" class="btn btn-success">Вхід</button>
      	<button type="button" class="btn btn-info"><a href="reg">Реєстрація</button>
   		<button type="button" class="btn btn-warning"><a href="reaccess">Забув пароль</a></button>
    </div>
  </div>
</form>
	</div>	
	
</body>
</html>