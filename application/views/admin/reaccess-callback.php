<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="/assets/css/bootstrap.css">
	<style>
		.content{
			margin:0 auto;
			width:360px;
			height:360px;
			border:1px solid black;
			padding:20px;
		}
		.logo img{
			margin-left:80px;			
			margin-bottom:20px;
		}
		a{
			color:white;
		}
	</style>
</head>
<body>
	<div class="content">
		<div class="logo">
			<img src="/assets/img/logo.png" alt="logo">
			<div class="form">
				<?php echo validation_errors();?>
				<?php echo form_open('admin/reaccess');?>
				<div class="form-group">
  					<label for="mail" class="col-sm-2 control-label">Email</label>
  					<div class="col-sm-10">
   						<input type="email" name="email" class="form-control" id="mail" placeholder="Email">
  					</div>
 				</div>
 				
 				<div class="form-group">
  					<div class="col-sm-offset-2 col-sm-10">
   						<button type="submit" class="btn btn-success">Відновити</button>
   						
  					</div>
 				</div>
				</form>	
			</div>
		</div>
		
		
	</div>
</body>
</html>