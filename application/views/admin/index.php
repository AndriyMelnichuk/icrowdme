<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.none1{
			min-width:30px;
			min-height:30px;
			background:red;
			display:inline;
		}
	</style>
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/common.js"></script>
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/site.css">
</head>
<body>

	<div class="topline">
		<a href="logout" class="logout"><img src="/assets/img/exit.png" alt=""></span></a>
	<div class="user">
		<img src="/assets/img/user.png" alt=""><?= $this->session->userdata('email');?>
	</div>
	
	
	</div>
	
	<div class="content">
					
	
		<div class="product">
			<span class="addspan" style = "cursor: pointer;"><img src="/assets/img/add.png" alt="">Add item</span>
			<div class="table">
				<table>
					<thead>
						<tr>
							<th>Name</th>
							<th>Link</th>
							<th>Control</th>
						</tr>
					</thead>
					<tbody class="list_menu">
						<?php foreach($array['menu'] as $product) :?>
						<tr>
							<td id="tdname"><?= $product->name;?></td>
							<td id="tdlink"><?= $product->link;?></td>
							<td id="conf">
								<input type="hidden" name="trId" value="0">
								<input type="hidden" name="id" value="<?= $product->id;?>">
								<img src="/assets/img/up.png" class="up" alt="">
								<img src="/assets/img/down.png" class="down" alt="">
								
								<img src="/assets/img/edit.png" class="edit" alt="">
								<img src="/assets/img/del.png" class="del" alt="">								
							</td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>

				<div class="logo">
				<center><h3>Logo</h3> </center>	
				<div class="left">
					<form enctype="multipart/form-data" method="post" action="/admin/updateLogo">
						<input type="file" class="file" name="userfile" data-button-text="Your label here."><br>
						<button class="btn btn-success" type="submit" name="sub">Change</button>
						<button class="btn btn-info default-logo">Default logo</button>
					</form>
				</div>
				<div class="right">
					<div class="logo_img">
							<?php foreach($array['logo'] as $img):?>
							<img src="/upload/<?= $img->name?>" alt="">
							<?php endforeach;?>
						</div>
				</div>
										
				</div>
				<div class="header">
				<h3><center>Header</center></h3> 
				<div class="left">
					<form enctype="multipart/form-data" method="post" action="/admin/updateHeader"> 
						<input type="file" name="userfile"><br>
						<button class="btn btn-success add-header" type="submit" name="sub">Add</button>
						<button class="btn btn-info default-header">Default header</button>
					</form>
				</div>
				</div>
					
					<table style="margin-top:50px">         
          				<tbody class="list_header">
            			<?php foreach($array['header'] as $header) :?>
            				<tr>
              					<td id="img"><img src="/upload/<?= $header->name;?>" alt=""></td>
              					<td id="hcaption"><?= $header->caption;?></td>
              					<td id="htext"><?= $header->text;?></td>
              					<td id="hlink"><?= $header->link;?></td>
              					<td id="hlinktext"><?= $header->linktext;?></td>              
              					<td id="conf">                
                						<input type="hidden" name="hid" value="<?= $header->id;?>">
               							<img src="/assets/img/up.png" class="hup" alt="">
						                <img src="/assets/img/down.png" class="hdown" alt="">
						                <img src="/assets/img/edit.png" class="hedit" alt="">              
						                <img src="/assets/img/del.png" class="hdel" alt="">                
              					</td>
            				</tr>
            			<?php endforeach;?>
          				</tbody>
        			</table>	
				</div>
			</div>
		</div>
	</div>
	<script>
	
	
	</script>

	<div id="editModal" class="modal fade" tabindex="-1" role="dialog">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title" >Edit menu</h4>
      			</div>
      			<div class="modal-body">
					<div class="alertsEdit"></div>	  
					<form class="formEditUser">      
    					<div class="form-group">
    						<label for="exampleInputPassword1">Name</label>
    						<input type="text" class="form-control" name="name" placeholder="Name">
  						</div>
    					<div class="form-group">
    						<label for="exampleInputPassword1">Name</label>
    						<input type="text" class="form-control" name="link" placeholder="Link">
  						</div>
  					</form>
  					<input type="hidden" name="hiddenEditId" value="0">
  					<input type="hidden" name="trIdForm" value="0">
  					<input type="hidden" name=hiddenId value="">
    				<button class="btn btn-success" id="submitEdit">Save change</button>
      			</div>
      		</div><!-- /.modal-content -->
  		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<div id="addModal" class="modal fade" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title" >Add item</h4>
      	</div>
     <div class="modal-body">
		<div class="alertsEdit"></div>	  
		<form class="formEditUser">      
    		<div class="form-group">
    			<label for="exampleInputPassword1">Name</label>
    			<input type="text" class="form-control" name="addname" placeholder="Name">
  			</div>
    		<div class="form-group">
    			<label for="exampleInputPassword1">Link</label>
    			<input type="text" class="form-control" name="addlink" placeholder="Link">
  			</div>
  		</form>  
    	<button class="btn btn-success" id="submitAdd">Save change</button>
     </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="addHeaderModal" class="modal fade" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title">Add header</h4>
      	</div>
     <div class="modal-body">
		<div class="alertsEdit"></div>	  
		<form enctype="multipart/form-data" method="post" action="/admin/updateHeader" class="form-header"> 						
    		<div class="form-group">
    			<label for="exampleInputPassword1">Image</label>
    			<input type="file" name="userfile" id="input-file-header"><br>     
  			</div>
    		<div class="form-group">
    			<label for="exampleInputPassword1">Carousel caption</label>
    			<input type="text" class="form-control" name="caption" placeholder="Carousel caption">
  			</div>
  			<div class="form-group">
    			<label for="exampleInputPassword1">Text</label>
    			<input type="text" class="form-control" name="text" placeholder="Text">
  			</div>
			<div class="form-group">
    			<label for="exampleInputPassword1">Link</label>
    			<input type="text" class="form-control" name="link" placeholder="Link">
  			</div>
  			<div class="form-group">
    			<label for="exampleInputPassword1">Link text</label>
    			<input type="text" class="form-control" name="linktext" placeholder="Link text">
  			</div>
  			
  			</form> 
  				<div class="form-group">
    			<button class="btn btn-success submit-new-header" type="submit" name="sub">Save</button>
  			</div>   	
     </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="editHeaderModal" class="modal fade" tabindex="-1" role="dialog">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title">Edit header</h4>
      	</div>
     <div class="modal-body">
		<div class="alertsEdit"></div>	  
		<form enctype="multipart/form-data" method="post" action="/admin/updateHeader" class="form-header"> 						
    		<div class="form-group">
    			<label for="exampleInputPassword1">Image</label>
    			<input type="file" name="userfile" id="input-file-header"><br>     
  			</div>
    		<div class="form-group">
    			<label for="exampleInputPassword1">Carousel caption</label>
    			<input type="text" class="form-control" name="hcaption" placeholder="Carousel caption">
  			</div>
  			<div class="form-group">
    			<label for="exampleInputPassword1">Text</label>
    			<input type="text" class="form-control" name="htext" placeholder="Text">
  			</div>
			<div class="form-group">
    			<label for="exampleInputPassword1">Link</label>
    			<input type="text" class="form-control" name="hlink" placeholder="Link">
  			</div>
  			<div class="form-group">
    			<label for="exampleInputPassword1">Link text</label>
    			<input type="text" class="form-control" name="hlinktext" placeholder="Link text">
  			</div>
  			
  			</form> 
  				<div class="form-group">
  				<input type="hidden" name = "hid">
    			<button class="btn btn-success submit-edit-header" type="submit" name="sub">Save</button>
  			</div>   	
     </div>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>


