<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registration</title>
	<link rel="stylesheet" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" href="/assets/css/form.css">
</head>
<body>
	<div class="content">
		<div class="logo">
			<center><img src="/assets/img/logo.png" alt="logo"></center>			
		</div>
		<?php echo validation_errors();?>
		<?php $attributes = array('class' => 'form-horizontal');?>
		<?php echo form_open('admin/reg', $attributes);?>
		<div class="form-group">
  				<label for="mail" class="col-sm-3 control-label">Email:</label>
  				<div class="col-sm-9">
   					<input type="email" name="email" class="form-control" id="mail" placeholder="Email">
  				</div>
 		</div>
 		<div class="form-group">
  				<label for="pass" class="col-sm-3 control-label">Password:</label>
  				<div class="col-sm-9">
   					<input type="password" name="password" class="form-control" id="pass" placeholder="Пароль">
  				</div>
 		</div>
 		<div class="form-group">
  			<div class="col-sm-offset-3 col-sm-9">
   				<button type="submit" class="btn btn-success">Зберегти</button>
   						
  			</div>
 		</div>
		</form>	
			</div>
		
		
	
</body>
</html>