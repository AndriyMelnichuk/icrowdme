
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
	<meta name="Author"    content="author">
	<meta name="Keywords" content="keywords">
	<meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Icrowdme</title>
  <link href="/assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/fonts.css">
	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="stylesheet" href="/assets/css/media.css">
  <style>
  
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="wrapper">
<!--=====HEADER=====-->
<header class="header">
  <div class="header__top-line">
    <!--TOP_NAV-->
    <nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="/">
            <?php foreach($mas['logo'] as $logo): ?>
              <img src="/upload/<?= $logo->name;?>" alt="logo">
            <?php endforeach; ?>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
          <ul class="nav navbar-nav navbar-right">
              <button type="button" class="btn  navbar-btn btn-login hover-ef">LOGIN</button>
          </ul>
          <ul class="nav navbar-nav navbar-right">          
            <?php foreach($mas['menu'] as $item): ?>
            <li><a href="<?php echo $item->link;?>"><?= $item->name;?> </a></li>            
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </nav>  
  </div>
  <!--carusel-->
  <div id="carousel-example-generic" class="carousel slide" >
    <div class="carousel-inner">
    <?php $i = 0; foreach($mas['header'] as $header):?>
      <?php $i+=1; if($i == 1):?>
      <div class="item active" style="background-image:url(/upload/<?= $header->name;?>)">
          <span class="carousel_caption"><?= $header->caption;?></span>
          <span><?= $header->text;?></span>
          <a href="<?= $header->link;?>" class="header-btn hover-ef" ><?= $header->linktext;?></a>
      </div>
    <?php endif;?>
    <?php if($i > 1):?>
      <div class="item" style="background-image:url(/upload/<?= $header->name;?>)">
      </div>
    <?php endif;?>
      <?php endforeach;?>
      
      
    </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
<!--end carusel-->
</header>
<!--=====END HEADER=====-->

<!--=====CONTENT=====-->
<section class="presentation">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <img src="/assets/img/icon-1.png" alt="icon">
        <h2>Our work is the presentation <br> of our capabilities.</h2>
        <p class="presentation-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
         <p class="presentation-text">Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
      </div>
    </div>
  </div>
</section>
<div class="radiotab">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs">
          <li class="active text-left">
            <a data-toggle="tab" href="#panel1">
              <span>1</span>
              <span>Simple</span>
            </a>
          </li>
          <li class="text-center">
            <a data-toggle="tab" href="#panel2">
              <span>2</span>
              <span>Transparent</span>
            </a>
          </li>
          <li class="text-right">
            <a data-toggle="tab" href="#panel3">
              <span>3</span>
              <span>Collaborative</span>
            </a>
          </li>
        </ul>
        <div class="tab-content">
          <div id="panel1" class="tab-pane fade in active">
          <div class="row">
            <div class="col-md-4">
              <div class="tabs-img text-center">
                <img src="/assets/img/glasses.png" alt="icon">
              </div>
            </div>
            <div class="col-md-8">
              <div class="tabs-text">
                <h3>Browse projects, like booking a hotel online !</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et do lore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>
          </div>
          <div id="panel2" class="tab-pane fade">
            <div class="row">
            <div class="col-md-4">
              <div class="tabs-img text-center">
                <img src="/assets/img/glasses.png" alt="icon">
              </div>
            </div>
            <div class="col-md-8">
              <div class="tabs-text">
                <h3>Browse projects, like booking a hotel online !</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et do lore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>
          </div>
          <div id="panel3" class="tab-pane fade">
            <div class="row">
            <div class="col-md-4">
              <div class="tabs-img text-center">
                <img src="/assets/img/glasses.png" alt="icon">
              </div>
            </div>
            <div class="col-md-8">
              <div class="tabs-text">
                <h3>Browse projects, like booking a hotel online !</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et do lore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="portfolio_picture">
<svg id="svg2" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" height="93.857" viewBox="0 0 110.36973 93.856796" width="110.37" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"><metadata id="metadata8"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/><dc:title/></cc:Work></rdf:RDF></metadata><defs id="defs6"><clipPath id="clipPath18" clipPathUnits="userSpaceOnUse"><path id="path16" d="m0 70.393h82.777v-70.393h-82.777z"/></clipPath><clipPath id="clipPath78" clipPathUnits="userSpaceOnUse"><path id="path76" d="m47.768 62.938h5.289v-5.2891h-5.289z"/></clipPath></defs><g id="g10" transform="matrix(1.3333 0 0 -1.3333 0 93.857)"><g id="g12"><g id="g14" clip-path="url(#clipPath18)"><g id="g20" transform="translate(11.338 11.598)"><path id="path22" d="m0 0v-6.596c0-0.519 0.574-0.939 1.146-0.939h39.475c0.76 0 1.24 0.42 1.24 0.939v6.596z" fill="#37919b"/></g><g id="g24" transform="translate(50.09 44.81)"><path id="path26" d="m0 0h-35.563c-1.761 0-3.189-1.428-3.189-3.189v-24.975c0-1.76 1.428-3.19 3.189-3.19h35.563c1.762 0 3.189 1.43 3.189 3.19v24.975c0 1.761-1.427 3.189-3.189 3.189" fill="#37919b"/></g><g id="g28" transform="translate(53.412 17.172)"><path id="path30" d="m0 0-38.885 0.002c-1.761 0-3.189-0.309-3.189-0.688v-6.601l38.367 0.135 3.518 0.193z" fill="#2c8484"/></g><g id="g32" transform="translate(1.5254 19.031)"><path id="path34" d="m0 0 10.387-2.43 3.24 5.098s3.381 3.478 1.668 7.195c-1.717 3.717-5.434 3.86-5.434 3.86l-9.48-6.625z" fill="#308a8e"/></g><g id="g36" transform="translate(53.41 17.174)"><path id="path38" d="m0 0v4.525h-38.258l-3.24-5.097z" fill="#30898e"/></g><g id="g40" transform="translate(12.006 21.84)"><path id="path42" d="m0 0c0 3.271-2.688 5.924-6.004 5.924-3.314 0-6.002-2.653-6.002-5.924 0-2.549 1.631-4.715 3.916-5.555v-14.609c0-2.234 8.09-2.234 8.09 0v19.734c0 0.049-0.014 0.096-0.014 0.145 0.004 0.094 0.014 0.189 0.014 0.285" fill="#37919b"/></g><g id="g44" transform="translate(63.178 19.031)"><path id="path46" d="m0 0-10.387-2.43-3.24 5.098s-3.383 3.478-1.668 7.195c1.717 3.717 5.432 3.86 5.432 3.86l9.482-6.625z" fill="#308a8e"/></g><g id="g48" transform="translate(52.697 21.84)"><path id="path50" d="m0 0c0 3.271 2.688 5.924 6.002 5.924 3.316 0 6.004-2.653 6.004-5.924 0-2.549-1.631-4.715-3.918-5.555v-14.609c0-2.234-8.088-2.234-8.088 0v19.734c0 0.049 0.014 0.096 0.014 0.145-0.004 0.094-0.014 0.189-0.014 0.285" fill="#37919b"/></g><g id="g52" transform="translate(82.596 1.5508)"><path id="path54" d="m0 0-8.438 4.52c-0.115 0.148-0.294 0.238-0.486 0.24-0.183 0.004-0.371-0.082-0.488-0.233l-8.871-4.572c-0.151-0.187-0.182-0.445-0.08-0.66 0.105-0.219 0.324-0.356 0.562-0.356h17.36c0.345 0 0.623 0.278 0.623 0.624 0 0.169-0.071 0.326-0.182 0.437" fill="#6a7872"/></g><g id="g56" transform="translate(73.781 2.7363)"><path id="path58" d="m0 0c-0.631 0-1.143 0.512-1.143 1.145v49.837c0.008 0.151 0.424 5.885-3.193 9.803-2.312 2.506-5.807 3.776-10.387 3.776-0.631 0-1.142 0.511-1.142 1.144s0.511 1.143 1.142 1.143c5.254 0 9.321-1.524 12.084-4.532 4.293-4.67 3.801-11.156 3.78-11.431l0.004-49.74c0-0.633-0.512-1.145-1.145-1.145" fill="#6a7872"/></g><g id="g60" transform="translate(73.781 22.766)"><path id="path62" d="m0 0c-1.094 0-1.982-0.887-1.982-1.982v-15.877c0.017-0.262 3.998-0.309 3.959 0.166l0.005 15.711c0 1.095-0.884 1.982-1.982 1.982" fill="#6a7872"/></g><g id="g64" transform="translate(74.83 3.1426)"><path id="path66" d="m0 0c0-0.592-0.48-1.072-1.072-1.072s-1.073 0.48-1.073 1.072 0.481 1.072 1.073 1.072 1.072-0.48 1.072-1.072" fill="#9dc3c5"/></g><g id="g68" transform="translate(74.465 3.1426)"><path id="path70" d="m0 0c0-0.391-0.318-0.707-0.707-0.707-0.391 0-0.705 0.316-0.705 0.707s0.314 0.707 0.705 0.707c0.389 0 0.707-0.316 0.707-0.707" fill="#8fb5b6"/></g><g id="g72"><g id="g86"><g id="g84" opacity=".5" clip-path="url(#clipPath78)"><g id="g82" transform="translate(53.057 60.293)"><path id="path80" d="m0 0c0-1.461-1.184-2.645-2.645-2.645-1.46 0-2.644 1.184-2.644 2.645s1.184 2.645 2.644 2.645c1.461 0 2.645-1.184 2.645-2.645" fill="#aed1d5"/></g></g></g></g><g id="g88" transform="translate(57.363 54.635)"><path id="path90" d="m0 0 1.658 13.697c0.024 0.188-0.043 0.377-0.178 0.512-0.128 0.133-0.324 0.203-0.509 0.18l-14.022-1.582c-0.238-0.028-0.441-0.192-0.519-0.416-0.078-0.229-0.02-0.479 0.154-0.649l12.354-12.183 0.003-0.004c0.247-0.242 0.639-0.239 0.881 0.006 0.117 0.123 0.182 0.283 0.178 0.439" fill="#aed1d5"/></g><g id="g92" transform="translate(56.631 68.393)"><path id="path94" d="m0 0c0 1.104 0.896 2 2.002 2 1.103 0 2-0.896 2-2 0-1.105-0.897-2.002-2-2.002-1.106 0-2.002 0.897-2.002 2.002" fill="#aed1d5"/></g></g></g></g></svg>
        </div>
        <div class="portfolio_text">
          <span class="portfolio-cap">Find Properties</span>
          <div class="prtfolio-des">Sign up to browse the portfolio. <br>
          Your journey to becoming a real<br> estate baron starts here.</div>
          <a href="#" class="portfolio-but hover-ef">Become A Member</a>
        </div> 
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="portfolio_picture">
<svg id="svg2" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" height="91.964" viewBox="0 0 99.994797 91.9636" width="99.995" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/"><metadata id="metadata8"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/><dc:title/></cc:Work></rdf:RDF></metadata><defs id="defs6"><clipPath id="clipPath18" clipPathUnits="userSpaceOnUse"><path id="path16" d="m0 68.973h74.996v-68.973h-74.996z"/></clipPath></defs><g id="g10" transform="matrix(1.3333 0 0 -1.3333 0 91.964)"><g id="g12"><g id="g14" clip-path="url(#clipPath18)"><g id="g20" transform="translate(74.402 9.0781)"><path id="path22" d="m0 0c0-4.297-1.008-4.215-5.51-4.215h-60.738c-4.504 0-8.154 3.485-8.154 7.781v36.526c0 4.297 3.65 7.783 8.154 7.783h60.303c4.504 0 5.945-1.541 5.945-5.838z" fill="#525252"/></g><g id="g24" transform="translate(72.816 10.375)"><path id="path26" d="m0 0c0-4.297-1.236-4.215-5.547-4.215h-57.879c-4.31 0-7.806 3.485-7.806 7.781v33.231c0 4.297 3.496 7.781 7.806 7.781h58.387c4.312 0 5.039-1.971 5.039-6.267z" fill="#444545"/></g><g id="g28" transform="translate(69.943 50.912)"><path id="path30" d="m0 0-51.225 6.832-2.955-22.166 51.225-6.828z" fill="#63bd6e"/></g><g id="g32" transform="translate(68.43 49.555)"><path id="path34" d="m0 0-48.609 6.48-2.547-19.099 48.611-6.479zm-2.178-19.678-49.484 6.598 2.685 20.142 49.485-6.599z" fill="#117f4a"/></g><g id="g36" transform="translate(51.594 42.08)"><path id="path38" d="m0 0c0.645 4.828-2.748 9.264-7.576 9.91-4.828 0.643-9.266-2.75-9.906-7.578-0.647-4.828 2.746-9.266 7.576-9.908 4.828-0.645 9.263 2.748 9.906 7.576" fill="#117f4a"/></g><g id="g40" transform="translate(42.709 38.522)"><path id="path42" d="m0 0c0.473 0.004 0.891 0.195 1.25 0.574 0.357 0.377 0.578 0.871 0.66 1.481 0.072 0.523 0.02 0.957-0.152 1.299-0.17 0.345-0.563 0.685-1.172 1.021zm0.619 10.021c-0.474-0.025-0.867-0.203-1.187-0.533-0.315-0.328-0.506-0.738-0.571-1.228-0.064-0.489-0.007-0.916 0.176-1.278 0.182-0.361 0.531-0.675 1.051-0.945zm-1.678-12.572 0.194 1.438c-0.586 0.166-1.051 0.39-1.391 0.676-0.342 0.283-0.615 0.699-0.816 1.246-0.207 0.546-0.282 1.201-0.227 1.959l1.233 0.099c-0.012-0.773 0.078-1.361 0.265-1.763 0.264-0.555 0.633-0.891 1.094-1.01l0.613 4.572c-0.476 0.174-0.949 0.471-1.422 0.887-0.347 0.301-0.591 0.691-0.734 1.17-0.145 0.478-0.178 1.007-0.102 1.584 0.137 1.023 0.545 1.81 1.229 2.361 0.465 0.369 1.1 0.551 1.9 0.543l0.092 0.687 0.707-0.095-0.094-0.686c0.68-0.179 1.2-0.496 1.551-0.953 0.455-0.592 0.678-1.353 0.666-2.281l-1.267-0.049c0.006 0.576-0.084 1.029-0.268 1.357-0.182 0.33-0.463 0.569-0.84 0.719l-0.553-4.146c0.577-0.254 0.956-0.45 1.141-0.584 0.346-0.246 0.615-0.526 0.807-0.84 0.195-0.315 0.33-0.672 0.404-1.076 0.076-0.403 0.084-0.828 0.025-1.274-0.136-1.002-0.507-1.799-1.123-2.392-0.613-0.594-1.341-0.858-2.181-0.789l-0.194-1.454z" fill="#63bd6e"/></g><g id="g44" transform="translate(56.086 68.973)"><path id="path46" d="m0 0-47.057-21.365 9.244-20.36 47.055 21.362z" fill="#63bd6e"/></g><g id="g48" transform="translate(55.523 67.018)"><path id="path50" d="m0 0-44.654-20.271 7.966-17.547 44.655 20.273zm8.586-17.84-45.461-20.637-8.398 18.502 45.459 20.637z" fill="#117f4a"/></g><g id="g52" transform="translate(45.211 51.756)"><path id="path54" d="m0 0c-2.014 4.436-7.242 6.398-11.68 4.385-4.435-2.014-6.398-7.242-4.382-11.678 2.013-4.436 7.242-6.4 11.677-4.385 4.436 2.012 6.399 7.242 4.385 11.678" fill="#117f4a"/></g><g id="g56" transform="translate(39.564 44.027)"><path id="path58" d="m0 0c0.398 0.252 0.646 0.635 0.752 1.148 0.103 0.51 0.029 1.045-0.227 1.608-0.214 0.48-0.488 0.82-0.814 1.019-0.328 0.202-0.84 0.284-1.537 0.245zm-4.791 8.824c-0.389-0.273-0.627-0.633-0.723-1.078-0.091-0.447-0.037-0.896 0.168-1.346 0.203-0.451 0.479-0.781 0.825-0.992 0.347-0.209 0.812-0.291 1.39-0.244zm5.242-11.547-0.597 1.319c-0.586-0.17-1.1-0.225-1.538-0.164-0.439 0.06-0.892 0.265-1.355 0.625-0.461 0.355-0.873 0.869-1.229 1.541l0.993 0.74c0.402-0.664 0.789-1.117 1.16-1.358 0.519-0.33 1.008-0.421 1.465-0.275l-1.909 4.199c-0.494-0.103-1.052-0.103-1.671 0-0.456 0.071-0.87 0.274-1.245 0.602-0.375 0.33-0.685 0.76-0.925 1.289-0.426 0.941-0.496 1.826-0.211 2.656 0.199 0.561 0.64 1.049 1.324 1.467l-0.285 0.631 0.648 0.295 0.287-0.631c0.668 0.207 1.278 0.211 1.817 0.014 0.701-0.262 1.295-0.789 1.775-1.584l-1.047-0.711c-0.304 0.49-0.619 0.824-0.949 1.007-0.328 0.184-0.693 0.239-1.092 0.165l1.729-3.809c0.623 0.09 1.049 0.125 1.277 0.109 0.422-0.025 0.801-0.119 1.131-0.285 0.332-0.16 0.633-0.394 0.912-0.695 0.279-0.303 0.51-0.658 0.695-1.07 0.418-0.92 0.524-1.793 0.319-2.622-0.209-0.83-0.686-1.439-1.434-1.826l0.606-1.334z" fill="#63bd6e"/></g><g id="g60" transform="translate(71,5.5117)"><path id="path62" d="m0 0c0-4.297-1.701-5.512-5.998-5.512h-60.627c-4.297 0-4.375 0.405-4.375 4.701v44.038s0.049 1.054 0.102 1.709c0.113 1.478 1.195 2.937 1.195 2.937s0.648-2.703 2.484-3.457c2.877-1.184 5.11-1.082 5.11-1.082h56.111c4.297 0 5.998-0.564 5.998-4.861z" fill="#616161"/></g><g id="g64" transform="translate(5.1348 45.898)"><path id="path66" d="m0 0l-4 0.002c-0.553 0-1 0.449-1 1 0 0.553 0.447 1 1 1l4-0.002c0.553 0 1-0.447 1-1s-0.447-1-1-1" fill="#717171"/></g><g id="g68" transform="translate(21.248 45.893)"><path id="path70" d="m0 0l-8.057 0.002c-0.552 0-1 0.449-1 1.002 0 0.551 0.448 0.998 1 0.998l8.057-0.002c0.553 0 1-0.449 1-1.002 0-0.551-0.447-0.998-1-0.998m16.113-0.008h-0.002l-8.056 0.004c-0.551 0-1 0.447-0.998 1 0 0.553 0.447 1 1 1l8.056-0.004c0.553 0 1-0.449 1-1-0.002-0.553-0.449-1-1-1m16.112-0.008l-8.057 0.004c-0.553 0-1 0.449-1 1 0 0.553 0.449 1 1 1h0.002l8.057-0.004c0.55 0 0.998-0.447 0.998-1 0-0.552-0.448-1-1-1m15.111-2.343c-0.424 0-0.818 0.273-0.953 0.699-0.283 0.898-0.772 1.64-3.871 1.64l-2.231 0.002c-0.552 0-1 0.448-1 1 0 0.553 0.448 1 1 1l2.231-0.002c2.502 0 4.933-0.367 5.779-3.037 0.166-0.527-0.127-1.089-0.652-1.256-0.102-0.031-0.203-0.046-0.303-0.046m0.309-16.082c-0.553 0-1 0.447-1 1v8.056c0 0.551 0.447 1 1 1 0.552 0 1-0.449 1-1v-8.056c0-0.553-0.448-1-1-1m0-16.114c-0.553 0-1 0.448-1 1v8.057c0 0.553 0.447 1 1 1 0.552 0 1-0.447 1-1v-8.057c0-0.552-0.448-1-1-1m-47.606-10.14h-8.057c-0.552 0-1 0.447-1 1 0 0.552 0.448 1 1 1h8.057c0.553 0 1-0.448 1-1 0-0.553-0.447-1-1-1m16.113 0h-8.056c-0.553 0-1 0.447-1 1 0 0.552 0.447 1 1 1h8.056c0.551 0 1-0.448 1-1 0-0.553-0.449-1-1-1m16.114 0h-8.057c-0.553 0-1 0.447-1 1 0 0.552 0.447 1 1 1h8.057c0.55 0 1-0.448 1-1 0-0.553-0.45-1-1-1m10.029 0h-1.975c-0.55 0-1 0.447-1 1 0 0.552 0.45 1 1 1h1.975c3.334 0 3.885 0.291 4.172 1.361 0.142 0.533 0.687 0.854 1.224 0.707 0.534-0.143 0.852-0.689 0.707-1.225-0.759-2.843-3.355-2.843-6.103-2.843" fill="#717171"/></g><g id="g72" transform="translate(5.1348,1.1973)"><path id="path74" d="m0 0h-4c-0.553 0-1 0.447-1 1s0.447 1 1 1h4c0.553 0 1-0.447 1-1s-0.447-1-1-1" fill="#717171"/></g><g id="g76" transform="translate(67.158 7.4121)"><path id="path78" d="m0 0c0-3.645-1.281-3.92-5.012-3.92h-62.039v42.221l62.239-0.028c3.734 0 4.812-1.031 4.812-4.673z" fill="#616161"/></g><g id="g80" transform="translate(74.537 22.748)"><path id="path82" d="m0 0h-19.453c-1.258 0-3.606 2.254-3.606 6.803s2.348 6.674 3.606 6.674h19.453c0.646 0 0.576-13.477 0-13.477" fill="#727172"/></g><g id="g84" transform="translate(74.537 21.668)"><path id="path86" d="m0 0h-19.453c-1.258 0-3.606 2.252-3.606 6.801s2.348 6.676 3.606 6.676h19.453c0.646 0 0.576-13.477 0-13.477" fill="#575656"/></g><g id="g88" transform="translate(60.139 28.367)"><path id="path90" d="m0 0c0-1.641-1.137-2.971-2.539-2.971s-2.539 1.33-2.539 2.971c0 1.643 1.137 2.973 2.539 2.973s2.539-1.33 2.539-2.973" fill="#d6cb78"/></g><g id="g92" transform="translate(59.644 28.367)"><path id="path94" d="m0 0c0-1.322-0.914-2.395-2.045-2.395s-2.045 1.073-2.045 2.395 0.914 2.396 2.045 2.396 2.045-1.074 2.045-2.396" fill="#ece17e"/></g></g></g></g></svg>
          </div>
          <div class="portfolio_text">
            <span class="portfolio-cap">Find Buyers</span>
            <div class="prtfolio-des">Sign up to browse the portfolio. <br>
            Your journey to becoming a real<br> estate baron starts here.</div>
            <a href="#" class="portfolio-but hover-ef">Submit Your Project</a>
        </div> 
      </div>
    </div>
  </div>
</div>
<section class="featured">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <div class="featured-wrap">
          <h4>Featured Projects</h4>
          <p class="featured_paragraphe">Brief details of new and popular projects. To view full listings, you will be required to <br> sign up and become a member.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="featured-wrap clearfix">
          <a href="#" class="hover-ef portfolio-but featured-but pull-right">Become a Member</a>
        </div> 
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <div class="featured-project featured-project_1">
          <div class="project-picture">
            <div class="project-picture-triagle"><span>New</span></div> 
            <div  class="project-picture-but  text-uppercase">From <span >GBP  3600</span></div>
          </div>
          <div class="project-text clearfix">
            <span class="project-cap">Ontario Tower ,<span class="text-transform"> LONDON</span></span>
            <p class="project-des">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
              </div>
            </div>
            <div class="progress-text clearfix">
              <span class="pull-left">10% Funded</span>
              <span class="pull-right">8 Days Left</span>
            </div>
            <hr>
            <a href="#" class="pull-right">See Project Details
              <span class="caret"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="featured-project featured-project_2">
          <div class="project-picture">
            <div class="project-picture-triagle"><span>New</span></div> 
            <div  class="project-picture-but  text-uppercase">From <span >GBP  3600</span></div>
          </div>
          <div class="project-text clearfix">
            <span class="project-cap">Ontario Tower ,<span class="text-transform"> LONDON</span></span>
            <p class="project-des">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
              </div>
            </div>
            <div class="progress-text clearfix">
              <span class="pull-left">60% Funded</span>
              <span class="pull-right">8 Days Left</span>
            </div>
            <hr>
            <a href="#" class="pull-right">See Project Details
              <span class="caret"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
        <div class="featured-project featured-project_3">
          <div class="project-picture">
            <div class="project-picture-triagle"><span>New</span></div> 
            <div class="project-picture-but  text-uppercase">From <span >GBP  3600</span></div>
          </div>
          <div class="project-text clearfix">
            <span class="project-cap">Ontario Tower ,<span class="text-transform"> LONDON</span></span>
            <p class="project-des">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
              </div>
            </div>
            <div class="progress-text clearfix">
              <span class="pull-left">10% Funded</span>
              <span class="pull-right">8 Days Left</span>
            </div>
            <hr>
            <a href="#" class="pull-right">See Project Details
              <span class="caret"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="review">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="h2 text-center">Be Part of a Growing International Community</h2>
        <p class="text-center">One of the challenges in networking is everybody thinks it's making cold calls to strangers. Actually, it's <br> the people who already have strong trust relationships with you</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-5">
        <div class="review_wraper text-center">
          <span class="review_cap">One of the Best </span>
          <p class="review_text">Lorem ipsum dolor sit amet, consectetur adipisicing <br> elit, sed do eiusmod taempor incididunt ut labore et<br> dolore magna aliqua.</p>
        </div>
        <div class="review_people">
          <img src="assets/img/people-1.jpg" alt="people">
          <span class="review_name">David Beckham</span>
        </div>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-5">
        <div class="review_wraper text-center">
          <span class="review_cap">Exellent Service </span>
          <p class="review_text">Lorem ipsum dolor sit amet, consectetur adipisicing <br> elit, sed do eiusmod taempor incididunt ut labore et<br> dolore magna aliqua.</p>
        </div>
        <div class="review_people">
          <img src="/assets/img/people-2.jpg" alt="people">
          <span class="review_name">David Beckham</span>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="partners">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="partners-wraper">
          <a href="#"><img src="/assets/img/logo-1.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-2.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-3.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-4.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-5.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-6.jpg" alt="logo" class="img-responsive"></a>
          <a href="#"><img src="/assets/img/logo-7.jpg" alt="logo" class="img-responsive"></a>
        </div>
      </div>
    </div>
  </div>
</div>
<!--=====END CONTENT=====-->

<!--====FOOTER====-->
<footer class="footer">
  <p class="text-center">Copyright &copy;  2001 - 2014  <a href="#">Cssauthor.com</a></p>
</footer>
<!--====END FOOTER====-->
</div>    

<script src="/assets/libs/JQ_1-9-1/jquery.min.js"></script>
<script src="/assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/common.js"></script>
</body>
</html>

