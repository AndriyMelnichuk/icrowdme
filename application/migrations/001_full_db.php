<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Full_db extends CI_Migration 
{
    public function up()
    {
        // Структура таблицы `roles`
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `header` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) DEFAULT NULL,
        
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
        
        // Дамп данных таблицы `roles`
        $this->db->query("
            INSERT INTO `header` (`id`, `name`) VALUES
        (1, 'header-bg.jpg');");
//----
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `logo` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) DEFAULT NULL,
        
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
        
        // Дамп данных таблицы `roles`
        $this->db->query("
            INSERT INTO `logo` (`id`, `name`) VALUES
        (1, 'logo.png');");
//----
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `menu` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) DEFAULT NULL,
        `visible` int(2) DEFAULT NULL,
        `link` varchar(255) DEFAULT NULL,
        
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
        
        // Дамп данных таблицы `roles`
        $this->db->query("
            INSERT INTO `menu` (`id`, `name`, `visible`, `link`) VALUES
        (1, 'HOME', 1, '#'),
        (2, 'HOW IT WORK', 1, '#'),
        (3, 'DISCOVER A PROJECT', 1, '#'),
        (4, 'LOG', 1, '#'),
        (5, 'FIND OUT MORE', 1, '#');");
//----
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `users` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email` varchar(255) DEFAULT NULL,
        `password` varchar(255) DEFAULT NULL,
       
        
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
        
        // Дамп данных таблицы `roles`
        $this->db->query("
            INSERT INTO `users` (`id`, `email`, `password`) VALUES
        
        (1, 'bmwman750i@gmail.com', '1');");
    }

    public function down()
    {
        // Для быстрого удаления
        // Сначала очищаем таблицу
        $this->db->query("TRUNCATE TABLE `header`");
        // Потом удаляем
        $this->db->query("DROP TABLE IF EXISTS `header`");

        $this->db->query("TRUNCATE TABLE `logo`");
        // Потом удаляем
        $this->db->query("DROP TABLE IF EXISTS `logo`");

        $this->db->query("TRUNCATE TABLE `menu`");
        // Потом удаляем
        $this->db->query("DROP TABLE IF EXISTS `menu`");

        $this->db->query("TRUNCATE TABLE `users`");
        // Потом удаляем
        $this->db->query("DROP TABLE IF EXISTS `users`");
    }
}