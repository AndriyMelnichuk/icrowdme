<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_position extends CI_Migration 
{
    public function up()
    {
        // Структура таблицы `roles`
        $this->db->query("ALTER TABLE `menu` ADD `position` int(11)");
        
        // Дамп данных таблицы `roles`
        $this->db->query("UPDATE `menu` SET `position`=1 ");
//----
    }
    public function down()
    {
        // Для быстрого удаления
        // Сначала очищаем таблицу
        $this->db->query("ALTER TABLE `menu` DROP `position`");
        // Потом удаляем
        
    }
}