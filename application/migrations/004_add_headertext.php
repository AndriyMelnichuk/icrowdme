<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_headertext extends CI_Migration 
{
    public function up()
    {
        // Структура таблицы `roles`
        $this->db->query("ALTER TABLE `header` ADD `caption` varchar(255)");
        $this->db->query("ALTER TABLE `header` ADD `text` varchar(255)");
        $this->db->query("ALTER TABLE `header` ADD `link` varchar(255)");
        $this->db->query("ALTER TABLE `header` ADD `linktext`  varchar(255)");
        $this->db->query("TRUNCATE TABLE `header`");
        
        
        // Дамп данных таблицы `roles`
       
//----
    }
    public function down()
    {
        // Для быстрого удаления
        // Сначала очищаем таблицу
        $this->db->query("ALTER TABLE `header` DROP `caption`");
        $this->db->query("ALTER TABLE `header` DROP `text`");
        $this->db->query("ALTER TABLE `header` DROP `link`");
        $this->db->query("ALTER TABLE `header` DROP `linktext`");
        // Потом удаляем
        
    }
}