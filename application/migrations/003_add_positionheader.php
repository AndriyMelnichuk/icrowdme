<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_positionheader extends CI_Migration 
{
    public function up()
    {
        // Структура таблицы `roles`
        $this->db->query("ALTER TABLE `header` ADD `position` int(11)");
        
        // Дамп данных таблицы `roles`
        $this->db->query("UPDATE `header` SET `position`=1 ");
//----
    }
    public function down()
    {
        // Для быстрого удаления
        // Сначала очищаем таблицу
        $this->db->query("ALTER TABLE `header` DROP `position`");
        // Потом удаляем
        
    }
}